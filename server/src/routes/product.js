
import { Router } from 'express';
import { ProductController } from '../controllers';

const router = Router();

router.post('/add-product', ProductController.createProduct);
router.post('/products', ProductController.getAllProducts);

export default router;