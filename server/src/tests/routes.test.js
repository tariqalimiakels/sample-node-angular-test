import request from 'supertest';
import app from '../server';

describe('Product Endpoints', () => {
  it('should create a new product', async () => {
    const res = await request(app)
      .post('/api/product/add-product')
      .send({
        name: 'New Product',
        price: 15000,
      });
    expect(res.statusCode).toEqual(201);
    expect(res.body).toHaveProperty('product');
  });

  it('should fetch all products', async () => {
    const res = await request(app).post('/api/product/products');
    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveProperty('products');
    expect(res.body.products.rows).toHaveLength(1);
  });

  it('should return status code 500 if db column value is violated', async () => {
    const res = await request(app)
      .post('/api/product/add-product')
      .send({
        name: 'New Product',
        price: 'One million',
      });
    expect(res.statusCode).toEqual(500);
    expect(res.body).toHaveProperty('error');
  });
});