import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import { ProductRouter } from '../routes';

const server = express();
server.use(express.json());
server.use(bodyParser.json())
server.use(bodyParser.json());
server.use(bodyParser.urlencoded({
  extended: false
}));
server.use(cors());

server.use('/api/product', ProductRouter);

export default server;