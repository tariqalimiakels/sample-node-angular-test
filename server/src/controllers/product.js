import models from "../database/models";
const Op = models.Sequelize.Op;
const pageSize = 5;

const createProduct = async (req, res) => {
  try {
    const product = await models.products.create(req.body);
    return res.status(201).json({
      product
    });
  } catch (error) {
    return res.status(500).json({
      error: error.message
    });
  }
};

const getAllProducts = async (req, res) => {
  const {
    offset,
    startDate,
    endDate
  } = req.body;

  const filter = {
    limit: pageSize,
    offset: offset,
    where: {}
  };

  //Search by date
  if (startDate && endDate) {

    filter['where']['createdAt'] = {
      [Op.between]: [startDate + ' 00:00:00', endDate + ' 23:59:59'],
    }
  } else if (startDate) {

    filter['where']['createdAt'] = {
      [Op.between]: [startDate + ' 00:00:00', startDate + ' 23:59:59'],
    }
  }

  try {
    const products = await models.products.findAndCountAll(filter);
    return res.status(200).json({
      products: products,
      pageSize: pageSize
    });
  } catch (error) {
    return res.status(500).send(error.message);
  }
};

export default {
  createProduct,
  getAllProducts,
};