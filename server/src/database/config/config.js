import dotenv from 'dotenv';
dotenv.config();

module.exports = {
  development: {
    username: process.env.DEV_USERNAME,
    password: process.env.DEV_PASSWORD,
    database: process.env.DEV_DATABASE,
    host: '127.0.0.1',
    port: 3306,
    dialect: 'mysql',
    timezone: process.env.DEV_TIMEZONE,
    dialectOptions: {
      bigNumberStrings: true
    },
    logging: false
  },
  test: {
    username: process.env.CI_DB_USERNAME,
    password: process.env.CI_DB_PASSWORD,
    database: process.env.CI_DB_NAME,
    host: '127.0.0.1',
    port: 3306,
    dialect: 'mysql',
    timezone: process.env.CI_DB_TIMEZONE,
    dialectOptions: {
      bigNumberStrings: true
    }
  },
  production: {
    username: process.env.PROD_DB_USERNAME,
    password: process.env.PROD_DB_PASSWORD,
    database: process.env.PROD_DB_NAME,
    host: process.env.PROD_DB_HOSTNAME,
    port: process.env.PROD_DB_PORT,
    dialect: 'mysql',
    timezone: process.env.PROD_DB_TIMEZONE,
    dialectOptions: {
      bigNumberStrings: true,
    }
  }
};