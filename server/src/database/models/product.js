
export default (sequelize, DataTypes) => {
  const Product = sequelize.define('products', {
    name: DataTypes.STRING,
    price: DataTypes.DECIMAL
  }, {});
  return Product;
};