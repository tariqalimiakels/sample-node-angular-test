module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.bulkInsert(
    'products',
    [
      {
        name: 'Tariq Product',
        price: 1500,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Mikaels Product',
        price: 2500,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ],
    {},
  ),

  down: (queryInterface, Sequelize) => queryInterface.bulkDelete('products', null, {}),
};
