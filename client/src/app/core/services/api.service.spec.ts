import { TestBed } from '@angular/core/testing';
import {HttpTestingController, HttpClientTestingModule} from '@angular/common/http/testing';

import { ApiService } from './api.service';
import { HttpClientModule } from '@angular/common/http';

describe('ApiService', () => {
  let service: ApiService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, HttpClientTestingModule],
          providers: [ApiService]
    });
    service = TestBed.inject(ApiService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('be able to retrieve products from the API via POST', () => {
    const dummyProducts: any[] = [{
        id: 1,
        name: 'Http Client',
        price: 'Testing Angular Service'
        }, {
        id: 2,
        body: 'Hello World2',
        title: 'Testing Angular Services'
    }];
    service.getProducts({}).subscribe(products => {
        expect(products).toEqual(dummyProducts);
    });

    const request = httpMock.expectOne( `${service.endpoint}/product/products`);
    expect(request.request.method).toBe('POST');
    request.flush(dummyProducts);
  });

  it('be able to post product from the API via POST', () => {
    const dummyProduct = {
      id: 1,
      name: 'Http Client',
      price: 'Testing Angular Service'
    };
    service.addProduct(dummyProduct).subscribe(product => {
        expect(product).toEqual(dummyProduct);
    });

    const request = httpMock.expectOne( `${service.endpoint}/product/add-product`);
    expect(request.request.method).toBe('POST');
    request.flush(dummyProduct);
  });

  afterEach(() => {
    httpMock.verify();
  });
  
});
