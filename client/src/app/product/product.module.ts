import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddProductComponent } from './add-product/add-product.component';
import { ProductsListComponent } from './products-list/products-list.component';
import { ProductRoutingModule } from './product-routing.module';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    AddProductComponent,
    ProductsListComponent,
  ],
  imports: [
    ProductRoutingModule,
    SharedModule,
    CommonModule,
  ]
})
export class ProductModule { }
