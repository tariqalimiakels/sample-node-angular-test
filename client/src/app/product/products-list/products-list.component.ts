import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../core/services/api.service';
import { Product } from 'src/app/interfaces';
import { PageEvent } from '@angular/material/paginator';
import * as moment from 'moment'
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.scss']
})
export class ProductsListComponent implements OnInit {

  displayedColumns: string[] = ['name', 'price', 'createdAt'];
  data: Product[] = [];
  isLoadingResults = true;
  totalItems = 0;
  pageSize = 0;
  startDate = new FormControl();
  endDate = new FormControl();
  
  constructor(private api: ApiService) { }

  ngOnInit(): void {
    this.loadProducts(0);
  }

  getNext(event: PageEvent) {
    const offset = event.pageSize * event.pageIndex
    this.loadProducts(offset);
  }

  search(){
    this.loadProducts(0);
  }

  private loadProducts(offset) {

    const startDate = moment(this.startDate.value).format('YYYY-MM-DD');
    const endDate = moment(this.endDate.value).format('YYYY-MM-DD');
    
    const data = {
      offset: offset
    };

    if(startDate !== "Invalid date"){
      data['startDate'] = startDate;
    }

    if(endDate !== "Invalid date"){
      data['endDate'] = endDate;
    }

    this.api.getProducts(data)
      .subscribe((res: any) => {
        const { rows, count } = res.products;
        this.totalItems = count;
        this.pageSize = res.pageSize;
        this.data = rows;
        this.isLoadingResults = false;
    }, err => {
        console.log(err);
        this.isLoadingResults = false;
    });
  }

}
