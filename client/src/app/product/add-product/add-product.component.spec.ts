import { async, ComponentFixture, TestBed, fakeAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { FormGroup, ReactiveFormsModule, FormsModule, FormBuilder } from '@angular/forms';
import {By} from "@angular/platform-browser";

import { ApiService } from '../../core/services/api.service';
import { AddProductComponent } from './add-product.component';

describe('AddProductComponent', () => {
  let component: AddProductComponent;
  let fixture: ComponentFixture<AddProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule, ReactiveFormsModule, FormsModule],
      declarations: [ AddProductComponent ],
      providers: [ApiService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('form invalid when empty', () => {
    expect(component.productForm.valid).toBeFalsy();
  });

  it("should not submit form'", fakeAsync(() => {
    spyOn(component, "onFormSubmit");

    fixture.whenStable().then(() => {
      expect(component.onFormSubmit).not.toHaveBeenCalled();
    });
  }));

  it('should submit a form', () => {
    spyOn(component, "onFormSubmit");

    expect(component.productForm.valid).toBeFalsy();
    component.productForm.controls['name'].setValue("test@test.com");
    component.productForm.controls['price'].setValue("123456789");
    expect(component.productForm.valid).toBeTruthy();

    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(component.onFormSubmit).toHaveBeenCalled();
    });
  });
});
